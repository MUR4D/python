numbers = [1, 2, 3, 4, 5]#initialized list

numbers1 = []#non-initialized list1
numbers2 = list()#non-initialized list1

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
numbers2 = list(numbers)

numbers = [1, 2, 3, 4, 5]
print(numbers[0])  # 1
print(numbers[2])  # 3
print(numbers[-3])  # 3

numbers[0] = 125  # change value of first element
print(numbers[0])  # 125

numbers = [5] * 6  # [5, 5, 5, 5, 5, 5]
print(numbers)

#range function
numbers = list(range(10))
print(numbers)      # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
numbers = list(range(2, 10))
print(numbers)      # [2, 3, 4, 5, 6, 7, 8, 9]
numbers = list(range(10, 2, -2))
print(numbers)      # [10, 8, 6, 4]

objects = [1, 2.6, "Hello", True]#different list

companies = ["Microsoft", "Google", "Oracle", "Apple"]
for item in companies:
    print(item)

companies = ["Microsoft", "Google", "Oracle", "Apple"]

i = 0
while i < len(companies):
    print(companies[i])
    i += 1

companies.append("xiaomi")
companies.clear()
companies.copy(numbers1)
companies.sort()