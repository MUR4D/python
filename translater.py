from googletrans import Translator
from googletrans import LANGUAGES

trans=Translator()
for lang in LANGUAGES:
    print(f'{lang}-{LANGUAGES[lang]}')

_from=input("from:")
text=input("Text:")
_to=input("to:")

t=trans.translate(text,_to,_from)

print(f'Source:{t.src}')
print(f'Dest:{t.dest}')
print(f'{t.origin}->{t.text}')